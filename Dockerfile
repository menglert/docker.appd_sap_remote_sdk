FROM alpine AS builder
LABEL maintainer="Michael Englert <michi.eng@gmail.com>"

ARG USER
ARG PASSWORD
ARG BASEURL="https://download.appdynamics.com/download/prox/download-file"
ARG VERSION

ENV APPD_HOME="/opt/appdynamics"

RUN apk update  
RUN apk upgrade
RUN apk add unzip curl
RUN mkdir -p /opt/appdynamics
RUN curl --referer http://www.appdynamics.com -c /tmp/cookies.txt -d "username=${USER}&password=${PASSWORD}" https://login.appdynamics.com/sso/login/
RUN curl -L -b /tmp/cookies.txt -o /tmp/sapagent.zip ${BASEURL}/sap-agent/${VERSION}/APPD-SAP-${VERSION}.zip
RUN unzip /tmp/sapagent.zip -d ${APPD_HOME}/sapagent
RUN mv ${APPD_HOME}/sapagent/APPD-SAP-*/SapAgent-*/appdhttpsdk/ ${APPD_HOME}/appdhttpsdk/
RUN chmod +x ${APPD_HOME}/appdhttpsdk/* 
RUN sed -i -e "s/\(.*\) nohup\(.*\)\(<&.*\)/\1\2#\3/" ${APPD_HOME}/appdhttpsdk/runSDKManager
RUN sed -i -e "s/\(.*\) \([>][>]*[ ]*\/dev\/null.*\)/\1 #\2/g" ${APPD_HOME}/appdhttpsdk/runSDKManager
RUN sed -i -e "s/#\!\/bin\/sh/#\!\/bin\/bash/" ${APPD_HOME}/appdhttpsdk/runSDKManager
RUN sed -i -e "s/#\!\/bin\/sh/#\!\/bin\/bash/" ${APPD_HOME}/appdhttpsdk/appd-http-sdk-proxy
RUN sed -i -e "s/#\!\/bin\/sh/#\!\/bin\/bash/" ${APPD_HOME}/appdhttpsdk/runAppdHttpSDK

FROM openjdk:8-jre-slim
LABEL maintainer="Michael Englert <michi.eng@gmail.com>"

ENV APPD_HOME="/opt/appdynamics"

RUN apt-get update \
    && apt-get install --fix-missing -q -y procps

COPY --from=builder ${APPD_HOME}/appdhttpsdk ${APPD_HOME}/appdhttpsdk

EXPOSE 7999 8900

CMD [ "/bin/bash", "-c", "${APPD_HOME}/appdhttpsdk/runSDKManager" ]