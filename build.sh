#!/bin/bash

echo -e "\x1B[7mSAP Agent Version\x1B[27m"
read sap_agent_version

echo -e "\x1B[7mImage Name\x1B[27m"
read image

echo -e "\x1B[7mPortal User\x1B[27m"
read user

echo -e "\x1B[7mPortal Password\x1B[27m"
read -s password

docker build \
--build-arg USER=$user \
--build-arg PASSWORD=$password \
--build-arg SAP_AGENT_VERSION=$sap_agent_version \
-t $image:latest .

exit 0
